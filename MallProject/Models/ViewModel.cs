﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace MallProject.Models
{
    public class ViewModel
    {

        public class MallOwnerViewModel
        {
            [Display(Name = "NO")]
            public int MallOwnerDetailsID { get; set; }
            [Display(Name = "Mall OwnerID")]
            public string MallOwnerID { get; set; }
            [Required]
            [Display(Name = "Owner Name")]
            public string OwnerName { get; set; }
            [Required]
            [Display(Name = "Address")]
            public string Address { get; set; }
            [Required]
            [Display(Name = "EmailID")]
            [EmailAddress]
            
            public string EmailID { get; set; }

            [Required]
            [Display(Name = "Date of Birth")]
            public String DOB { get; set; }


            [Display(Name = "Age")]
            public Nullable<int> Age { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
            [Display(Name = "Phone.No")]
            public string PhnNo { get; set; }


            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
            [Display(Name = "Image")]
            public string Img { get; set; }
            public Nullable<System.DateTime> RegDate { get; set; }
           
            [Display(Name = "Gender")]
            public string Gender { get; set; }


        }
        public class CategoryViewModel
        {

            [Display(Name = "Category")]
            public int MallCategoryID { get; set; }

            [Required]
            [Display(Name = "SQFT Range")]
            public string MallCategoryName { get; set; }

            [Required]
            [Display(Name = "Category Name")]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Amount")]
            public Nullable<decimal> Amount { get; set; }
        }
        public class MallEmpViewModel
        {
            [Display(Name = "NO")]
            public int MallEmpDetailsID { get; set; }
            [Display(Name = "ID")]
            public string MallEmpID { get; set; }
            [Required]
            [Display(Name = "Name")]

            public string EmpName { get; set; }
            [Required]
            [Display(Name = "Mall ")]
            public int MallID { get; set; }
            [Required]
            [Display(Name = "Post")]
            public int EmpPostID { get; set; }
            [Required]
            [Display(Name = "Address")]
            public string Address { get; set; }
            [Required]
            [Display(Name = "EmailID")]
            [EmailAddress]
            public string EmailID { get; set; }
            [Required]
            [Display(Name = "Date of Birth")]
            public String DOB { get; set; }

            [Required]
            [Display(Name = "Age")]
            public Nullable<int> Age { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
            [Display(Name = "Phone.No")]
            public string PhnNo { get; set; }


            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }


            [Required]
            [Display(Name = "Salary")]
            public Nullable<decimal> Salary { get; set; }
            public string Post { get; set; }

            public List<MallEmpDetailsTbl> MallEmpDetailsTbls { get; set; }
            public MallEmpDetailsTbl SelectedEmp { get; set; }
            public string DisplayMode { get; set; }

            public string Img { get; set; }
            public Nullable<System.DateTime> RegDate { get; set; }
            [Display(Name = "Mall Name")]
            public string MallName { get; set; }
            [Required]
            [Display(Name = "Gender")]
            public string Gender { get; set; }
        }

        public class MallViewModel
        {


            public int MallID { get; set; }

            public string MallOwnerID { get; set; }
            [Required]
            [Display(Name = "Mall Name")]
            public string MallName { get; set; }
            [Required]
            [Display(Name = "Square Feet")]
            public Nullable<decimal> SQFT { get; set; }
            [Required]
            [Display(Name = "No Of Rooms")]
            public Nullable<int> NRooms { get; set; }
            [Required]
            [Display(Name = "Number of Floors")]
            public Nullable<int> NFloor { get; set; }

            public string Img { get; set; }

            public Nullable<System.DateTime> ExpiryDate { get; set; }

            [Required]
            [Display(Name = "Licence No")]
            public string LicenceNo { get; set; }
            [Required]
            [Display(Name = "Address")]
            public string Address { get; set; }
            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
            [Display(Name = "Phone No")]
            public string PhnNo { get; set; }
            public Nullable<System.DateTime> RegDate { get; set; }

            //Category Tbls...
            [Required]
            [Display(Name = "Category")]
            public int MallCategoryID { get; set; }

            //[Display(Name = "SQFT Range")]
            //public string MallCategoryName { get; set; }

            //[Display(Name = "Category Name")]         
            //public string Name { get; set; }

            //[Display(Name = "Amount")]
            //public Nullable<decimal> Amount { get; set; }
            //.............
            public List<MallTbl> MallTbls { get; set; }
            public MallTbl SelectedMall { get; set; }
            public string DisplayMode { get; set; }


        }

        public class AdminMallMsgModel
        {


            public int AdminMallMsgID { get; set; }
            [Display(Name = "Mall")]
            public int MallID { get; set; }
            public Nullable<bool> WhoStatus { get; set; }
            public Nullable<System.DateTime> DatePosted { get; set; }
            [Required]
            [Display(Name = "Message")]
            public string Msg { get; set; }
            public Nullable<bool> MallViewStatus { get; set; }
            public Nullable<bool> AdminViewStatus { get; set; }
            public Nullable<bool> ReadStatus { get; set; }
        }

        public class MallEmpMsgModel
        {
            public int MallEmpMsgID { get; set; }
            public string MallEmpID { get; set; }
            public Nullable<bool> WhoStatus { get; set; }
            public Nullable<System.DateTime> DatePosted { get; set; }
            [Required]
            [Display(Name = "Message")]
            public string Msg { get; set; }
            public Nullable<bool> MallViewStatus { get; set; }
            public Nullable<bool> EmpViewStatus { get; set; }
            public Nullable<bool> ReadStatus { get; set; }
        }
        public class AdminMallTransactionModel
        {
     
            [Display(Name = "TransactionID")]          
            public int AdminMallTransactionID { get; set; }        
            [Required]
            [Display(Name = "Mall")]
            public int MallID { get; set; }
            [Display(Name = "Amount")]
            public Nullable<decimal> Amount { get; set; }
            [Display(Name = "Date of Payment")]
            public Nullable<System.DateTime> DatePaid { get; set; }
            [Required]
            [Display(Name = "Payment Mode")]
            public string PaymentMode { get; set; }
            public Nullable<bool> AdminViewStatus { get; set; }
            public Nullable<bool> MallViewStatus { get; set; }
        }
      
        public class MallEmpPayModel
        {
            [Display(Name = "TransactionID")]
            public int MallEmpPayID { get; set; }
            [Required]
            [Display(Name = "Employee ID")]
            public string MallEmpID { get; set; }
            [Display(Name = "Amount")]
            public Nullable<decimal> Amount { get; set; }
            [Display(Name = "Date of Payment")]
            public Nullable<System.DateTime> DatePaid { get; set; }
            [Required]
            [Display(Name = "Payment Mode")]
            public string PaymentMode { get; set; }
            public Nullable<bool> OwnerViewStatus { get; set; }
            public Nullable<bool> EmpViewStatus { get; set; }

            [Display(Name = "Mall ")]
            public int MallID { get; set; }
        }
        public class EventViewModel
        {
            public int EventID { get; set; }
            [Display(Name = "Date")]
            [Required]
            public String Date { get; set; }

            [Required]
            [Display(Name = "Event name")]
            public string EventName { get; set; }
            [Required]
            [Display(Name = "Mall")]
            public int MallID { get; set; }
            [Required]

            [Display(Name = "Person Name")]
            public string PersonName { get; set; }
            [Required]
            [Display(Name = "Address")]
            public string Address { get; set; }
            [Required]
            [Display(Name = "Phone No")]
            public string PhnNo { get; set; }
            [Required]
            [Display(Name = "Amount")]
            public Nullable<decimal> Amount { get; set; }
            public class EventViewModel2
            {
                [Display(Name = "DATE:")]
                public String Date { get; set; }

                [Required]
                [Display(Name = "MALL:")]
                public int MallID { get; set; }

            }

        }
    }
}

