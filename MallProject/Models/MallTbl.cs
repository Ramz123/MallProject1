//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MallProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MallTbl
    {
        public int MallID { get; set; }
        public int MallCategoryID { get; set; }
        public string MallOwnerID { get; set; }
        public string MallName { get; set; }
        public Nullable<decimal> SQFT { get; set; }
        public Nullable<int> NRooms { get; set; }
        public Nullable<int> NFloor { get; set; }
        public string LicenceNo { get; set; }
        public string Address { get; set; }
        public string PhnNo { get; set; }
        public string Img { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public Nullable<System.DateTime> RegDate { get; set; }
    }
}
