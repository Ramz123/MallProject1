//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MallProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ShopOwnerDetailsTbl
    {
        public int ShopOwnerDetailsID { get; set; }
        public string ShopOwnerID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Nullable<int> Age { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string EmailID { get; set; }
        public string PhnNo { get; set; }
        public string img { get; set; }
    }
}
