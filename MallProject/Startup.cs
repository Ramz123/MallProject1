﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MallProject.Startup))]
namespace MallProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
