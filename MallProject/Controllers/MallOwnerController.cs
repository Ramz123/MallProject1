﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MallProject.Models;
using static MallProject.Models.ViewModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;

namespace MallProject.Controllers
{
    public class MallOwnerController : Controller
    {
       
        static string pre = "";
        static string loc1 = "";
        
        static object lockObj = new object();//for remains value static ...
        MallManagementEntities db = new MallManagementEntities();
        //For registration..
        #region Registration files

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public MallOwnerController()
        {
        }

        public MallOwnerController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion
        #region Helpers
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
        //Registration files end


        #region//Registration Actions..

        public ActionResult MallOwnerReg()
        {

            return View();

        }


        [HttpPost]
        public async Task<ActionResult> MallOwnerReg(MallOwnerViewModel DetailsPost, HttpPostedFileBase MyImage)
        {

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = DetailsPost.EmailID, Email = DetailsPost.EmailID };
                var result = await UserManager.CreateAsync(user, DetailsPost.Password);
                if (result.Succeeded)
                {
                    // await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    UserManager.AddToRole(user.Id, "MallOwner");
                    //ViewBag.abc = "Registered Succesffully";
                    MallOwnerDetailsTbl NewAdd = new MallOwnerDetailsTbl();
                    NewAdd.MallOwnerID = user.Id;

                    NewAdd.OwnerName = DetailsPost.OwnerName;
                    NewAdd.Address = DetailsPost.Address;
                    NewAdd.EmailID = DetailsPost.EmailID;
                   
                    if (DetailsPost.DOB != null && DetailsPost.DOB != "")
                    {
                        var Date = DateTime.ParseExact(DetailsPost.DOB, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        NewAdd.DOB = Date;
                    }
                    NewAdd.Age = DetailsPost.Age;
                    NewAdd.Gender = DetailsPost.Gender;
                    NewAdd.PhnNo = DetailsPost.PhnNo;
                    NewAdd.RegDate = DateTime.Now;
                    db.MallOwnerDetailsTbls.Add(NewAdd);
                    db.SaveChanges();

                    //for image upload
                    if (MyImage != null && MyImage.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(MyImage.FileName);

                        fileName = NewAdd.MallOwnerID + "-MallOwner-Image-" + fileName;

                        var path = Path.Combine(Server.MapPath("~/Uploads/MallOwner"), fileName);

                        MyImage.SaveAs(path); //image store in server

                        NewAdd.Img = fileName; //imgage path,db
                    }
                    db.SaveChanges();
                    TempData["Message"] = "Registered Successfully!";
                    // ViewBag.abc = NewAdd.MallOwnerID;
                    return RedirectToAction("Login", "Account");
                }
                AddErrors(result);
            }
            //  return View(DetailsPost);
            return View();

        }

        public ActionResult MallReg()
        {
            ViewBag.MallCategoryVar = db.MallCategoryTbls.ToList();
            return View();

        }



        [HttpPost]
        [Authorize(Roles = "MallOwner")]
        public ActionResult MallReg(MallViewModel DetailsPost, HttpPostedFileBase MyImage)
        {

            if (ModelState.IsValid)
            {
                //ViewBag.abc = "Registered Succesffully";
                MallTbl NewAdd = new MallTbl();
                var MallOwnerID = User.Identity.GetUserId();
                // NewAdd.MallID = DetailsPost.MallID;
                NewAdd.MallOwnerID = MallOwnerID;

                NewAdd.MallName = DetailsPost.MallName;
                // NewAdd.DOB = Common.CommonFunctions.GetCurrentDateTime(); 
                NewAdd.SQFT = DetailsPost.SQFT;
                NewAdd.NRooms = DetailsPost.NRooms;
                NewAdd.NFloor = DetailsPost.NFloor;
                NewAdd.MallCategoryID = DetailsPost.MallCategoryID;
                NewAdd.LicenceNo = DetailsPost.LicenceNo;
                NewAdd.Address = DetailsPost.Address;
                NewAdd.PhnNo = DetailsPost.PhnNo;
                NewAdd.RegDate = DateTime.Now;
                DateTime date = DateTime.Now;
                NewAdd.ExpiryDate = date.AddYears(1);
                NewAdd.Img = DetailsPost.Img;
                db.MallTbls.Add(NewAdd);
                db.SaveChanges();


                if (MyImage != null && MyImage.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(MyImage.FileName);

                    fileName = NewAdd.MallID + "-Mall-Image-" + fileName;

                    var path = Path.Combine(Server.MapPath("~/Uploads/Mall"), fileName);

                    MyImage.SaveAs(path); //image store in server

                    NewAdd.Img = fileName; //imgage path,db
                }
                db.SaveChanges();
                TempData["Message"] = "Registered Successfully!";
                return RedirectToAction("MallOwnerIndex");
            }

            ViewBag.MallCategoryVar = db.MallCategoryTbls.ToList();
            return View(DetailsPost);
        }

        [Authorize(Roles = "MallOwner")]
        public ActionResult MallEmpReg()
        {
            //for drop down list

            ViewBag.EmpPostVar = db.EmpPostTbls.ToList();
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            return View();

        }

        [HttpPost]
        [Authorize(Roles = "MallOwner")]
        public async Task<ActionResult> MallEmpReg(MallEmpViewModel DetailsPost, HttpPostedFileBase MyImage)
        {
            if (ModelState.IsValid)
            {

                var user = new ApplicationUser { UserName = DetailsPost.EmailID, Email = DetailsPost.EmailID };
                var result = await UserManager.CreateAsync(user, DetailsPost.Password);
                if (result.Succeeded)
                {

                    UserManager.AddToRole(user.Id, "MallEmp");

                    //ViewBag.abc = "Registered Succesffully";
                    MallEmpDetailsTbl NewAdd = new MallEmpDetailsTbl();
                    MallTbl Newmall = new MallTbl();
                    NewAdd.MallEmpID = user.Id;
                    NewAdd.EmpName = DetailsPost.EmpName;
                    NewAdd.Salary = DetailsPost.Salary;
                    NewAdd.MallID = DetailsPost.MallID;
                    NewAdd.EmpPostID = DetailsPost.EmpPostID;
                    NewAdd.Address = DetailsPost.Address;
                    NewAdd.EmailID = DetailsPost.EmailID;

                    if(DetailsPost.DOB!=null && DetailsPost.DOB != "")
                    {
                        var Date = DateTime.ParseExact(DetailsPost.DOB, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        NewAdd.DOB = Date;
                    }
                    //   NewAdd.DOB = Common.CommonFunctions.GetCurrentDateTime();
                    
                    NewAdd.Age = DetailsPost.Age;
                    NewAdd.Gender = DetailsPost.Gender;
                    NewAdd.PhnNo = DetailsPost.PhnNo;
                    NewAdd.RegDate = DateTime.Now;
                    db.MallEmpDetailsTbls.Add(NewAdd);
                    db.SaveChanges();
                    //for image............................
                    if (MyImage != null && MyImage.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(MyImage.FileName);

                        fileName = NewAdd.MallEmpID + "-MallEmp-Image-" + fileName;

                        var path = Path.Combine(Server.MapPath("~/Uploads/MallEmp"), fileName);

                        MyImage.SaveAs(path); //image store in server

                        NewAdd.Img = fileName; //imgage path,db
                    }
                    db.SaveChanges();
                    TempData["Message"] = "Registered Successfully!";

                    //  ViewBag.abc = "Registered Succesffully";
                    return RedirectToAction("MallOwnerIndexEmpSpace");
                }
                AddErrors(result);
            }

            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            ViewBag.EmpPostVar = db.EmpPostTbls.ToList();
            return View(DetailsPost);
            // return View();
        }
        #endregion

        #region Event view or add...
        public ActionResult ViewEvent()
        {
           
            var MallOwnerID = User.Identity.GetUserId();
            var EventList = db.EventTbls.ToList();
            
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            foreach(var id in MallList)
            {
                foreach(var id2 in EventList)
              {
                    if (id.MallID==id2.MallID)
                    {
                       
                        var abc = db.EventTbls.Where(x => x.MallID == id2.MallID).ToList() ;

                        ViewBag.Date = abc;
                        ViewBag.Mallvar = MallList;
                        return View();
                    }
                    
                }

            }
            return RedirectToAction("AddEvent");    
           
           
        }
        [HttpPost]
        public ActionResult ViewEvent(EventViewModel DetailsPost)
        {
            int MallID = DetailsPost.MallID;
           string Date = DetailsPost.Date;
         
            return RedirectToAction("ViewEvent", new { MallID = MallID, Date=Date});
        }
        public ActionResult AddEvent()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            return View();
        }

        [HttpPost]
        public ActionResult AddEvent(EventViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                EventTbl NewAdd = new EventTbl();
                if (DetailsPost.Date != null && DetailsPost.Date != "")
                {
                    var Date = DateTime.ParseExact(DetailsPost.Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    NewAdd.Date = Date;
                }
                NewAdd.EventName = DetailsPost.EventName;
                NewAdd.MallID = DetailsPost.MallID;
                NewAdd.Amount = DetailsPost.Amount;
                NewAdd.PersonName = DetailsPost.PersonName;
                NewAdd.Address= DetailsPost.Address;
                NewAdd.PhnNo = DetailsPost.PhnNo;
                db.EventTbls.Add(NewAdd);
                db.SaveChanges();
                TempData["Message"] = "Added Successfully!";//for alert message
                return RedirectToAction("ViewEvent");
            }
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            var EventList = db.EventTbls.ToList();
            foreach (var id in MallList)
            {
                foreach (var id2 in EventList)
                {
                    if (id.MallID == id2.MallID)
                    {
                        ViewBag.Date = EventList;
                    }
                }

            }
            return View();
          
        }
        #endregion

        

        #region (Login form no need)..
        public ActionResult MallOwnerLogin()
        {
            return View();

        }

        public ActionResult MallEmpLogin()
        {
            return View();

        }
        [HttpPost]
        public ActionResult MallEmpLogin(MallEmpViewModel DetailsPost)
        {

            return RedirectToAction("MallEmpIndex");
        }
        #endregion


        #region( Login Index actions..)
        [Authorize(Roles = "MallOwner")]
        public ActionResult MallOwnerIndex()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallOwnerDetailsTbls.Where(x => x.MallOwnerID == MallOwnerID).FirstOrDefault();
            ViewBag.Name = MallList.OwnerName;
            return View();

        }
        [Authorize(Roles = "MallEmp")]
        public ActionResult MallEmpIndex()
        {
           ViewBag.MallEmpID = User.Identity.GetUserId();
            return View();

        }

        public ActionResult MallSite(int MallID)
        {
            var abc = MallID;
            // ViewBag.name = abc;
            var MallSite = db.MallTbls.Where(x => x.MallID == abc).FirstOrDefault();

            return View(MallSite);


        }
        #endregion

        #region(Table View actions..)

        //employee list view actions..

        [Authorize(Roles = "MallOwner")]
        public ActionResult EmpListViewSearch()
        {

            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            ViewBag.EmpPostVar = db.EmpPostTbls.ToList();
            return View();
        }


        [HttpPost]
        public ActionResult EmpListViewSearch(MallEmpViewModel DetailsPost)
        {
            int MallID = DetailsPost.MallID;
            String Post = DetailsPost.Post;
            String EmpName = DetailsPost.EmpName;
            return RedirectToAction("EmpListViewSearch", new { MallID = MallID, Post = Post, EmpName = EmpName });
        }


        //employee profile view action..
       
        public ActionResult EmpProfileView(string MallEmpID)
        {
            var EmpProfileView = db.MallEmpDetailsTbls.Where(x => x.MallEmpID == MallEmpID).FirstOrDefault();
            string pre2 = Request.UrlReferrer.ToString();//for geting url linked to this url
           
            if (pre2.Contains("EmpProfileEdit") == false)//checking a string contains a particular word
            {
                lock (lockObj)
                {
                    pre = Request.UrlReferrer.ToString();//assigning url linked to this url
                    ViewBag.pre = pre2;
                }
            }
            else
            {
                ViewBag.pre = pre;
            }
            return View(EmpProfileView);

        }
        

        [Authorize(Roles = "MallOwner")]
        public ActionResult MallOwnerProfileView()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallOwnerProfileView = db.MallOwnerDetailsTbls.Where(x => x.MallOwnerID == MallOwnerID).FirstOrDefault();

            return View(MallOwnerProfileView);
        }

        //Mall List of particular mall owner

        [Authorize(Roles = "MallOwner")]
        public ActionResult MallOwnerMallList()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallOwnerMallListView = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).OrderByDescending(x => x.RegDate).ToList();
            return View(MallOwnerMallListView);

        }
        public ActionResult ViewOwnerEmpPay()
        {
            if (User.IsInRole("MallOwner"))
            {
                var MallOwnerID = User.Identity.GetUserId();
                var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
                ViewBag.Mallvar = MallList;
                return View();
            }
            else
            {
                return View();
            }

        }
        [HttpPost]
        public ActionResult ViewOwnerEmpPay(MallEmpPayModel DetailsPost)
        {
            int MallID = DetailsPost.MallID;
            
            return RedirectToAction("ViewOwnerEmpPay", new { MallID = MallID});
        }
        #endregion

        #region (Remove Actions)
        public ActionResult RemoveMall(int MallID)
        {
            MallTbl p = db.MallTbls.Find(MallID);
            db.MallTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            return RedirectToAction("MallOwnerMallList", new { MallID = 0 });
        }
        public ActionResult RemoveEmp(string MallEmpID)
        {
            MallEmpDetailsTbl p = db.MallEmpDetailsTbls.Find(MallEmpID);
            db.MallEmpDetailsTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            return RedirectToAction("EmpListViewSearch", new { MallEmpID = 0 });
        }

        //Remove admin mallowner Message...........
        [HttpPost]
        public ActionResult RemoveMsg(int[] ids)
        {
            foreach (var id in ids)//checkbox checking
            {
                AdminMallMsgTbl NewAdd = db.AdminMallMsgTbls.Find(id);
                NewAdd.MallViewStatus = false;

                db.SaveChanges();

                AdminMallMsgTbl p = db.AdminMallMsgTbls.Find(id);
                if (NewAdd.MallViewStatus == NewAdd.AdminViewStatus)
                {
                    db.AdminMallMsgTbls.Remove(p);
                    db.SaveChanges();

                }
            }
            TempData["Message"] = "Deleted Successfully!";
            return RedirectToAction("ComplaintToAdmin", new { id = 0 });
        }

        //Remove  mallowner Emp Message...........
        [HttpPost]
        public ActionResult RemoveMsg2(int[] ids)
        {


            foreach (var id in ids)//checkbox checking
            {
                MallEmpMsgTbl NewAdd = db.MallEmpMsgTbls.Find(id);

                if (User.IsInRole("MallOwner"))
                {
                    NewAdd.MallViewStatus = false;
                }
                else if (User.IsInRole("MallEmp"))
                {
                    NewAdd.EmpViewStatus = false;
                }

                db.SaveChanges();

                MallEmpMsgTbl p = db.MallEmpMsgTbls.Find(id);
                if (NewAdd.MallViewStatus == NewAdd.EmpViewStatus)
                {
                    db.MallEmpMsgTbls.Remove(p);
                    db.SaveChanges();

                }
            }
            TempData["Message"] = "Deleted Successfully!";
            return Redirect(Request.UrlReferrer.ToString());
            // return RedirectToAction("RemarkToEmpPage", new { id = 0 });
        }
        //to remove payment list....
        [HttpPost]
        public ActionResult RemoveOwnerEmpPay(int[] ids)
        {


            foreach (var id in ids)
            {
                MallEmpPayTbl NewAdd = db.MallEmpPayTbls.Find(id);
                if (User.IsInRole("MallOwner"))
                {

                    NewAdd.OwnerViewStatus = false;
                }
                else
                {
                    NewAdd.EmpViewStatus = false;
                }

                db.SaveChanges();

                MallEmpPayTbl p = db.MallEmpPayTbls.Find(id);
                if (NewAdd.OwnerViewStatus == NewAdd.EmpViewStatus)
                {
                    db.MallEmpPayTbls.Remove(p);
                    db.SaveChanges();

                }
            }
            TempData["Message"] = "Deleted Successfully!";
            //return RedirectToAction("ViewOwnerEmpPay");
            return Redirect(Request.UrlReferrer.ToString());

        }

        public ActionResult RemoveEvent(int EventID)
        {
            EventTbl p = db.EventTbls.Find(EventID);
            db.EventTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            return Redirect(Request.UrlReferrer.ToString());

        }



        #endregion

        #region MallIndex no need
        public ActionResult MallIndex(int MallID)
        {
            ViewBag.abc = MallID;
            var MallID1 = MallID;
            return View(MallID1);
        }
        #endregion



        #region (MallOwnerIndex button actions..

        public ActionResult MallOwnerIndexAdminSpace()
        {
            return View();
        }
        public ActionResult MallOwnerIndexShopSpace()
        {
            return View();
        }
        public ActionResult MallOwnerIndexEmpSpace()
        {
            return View();
        }
        #endregion


        #region (Edit actions)

        //Mall Owner Mall Edit

        [Authorize(Roles = "MallOwner")]
        public ActionResult MallOwnerMallEdit(int MallID)
        {
            var CheckDetails = db.MallTbls.Where(x => x.MallID == MallID).FirstOrDefault();
            if (CheckDetails != null)
            {
                MallViewModel EditMall = new MallViewModel();
                EditMall.MallName = CheckDetails.MallName;
                EditMall.SQFT = CheckDetails.SQFT;
                EditMall.NRooms = CheckDetails.NRooms;
                EditMall.NFloor = CheckDetails.NFloor;
                EditMall.MallCategoryID = CheckDetails.MallCategoryID;
                EditMall.LicenceNo = CheckDetails.LicenceNo;
                EditMall.Address = CheckDetails.Address;
                EditMall.PhnNo = CheckDetails.PhnNo;
                EditMall.Img = CheckDetails.Img;
                ViewBag.MallCategoryVar = db.MallCategoryTbls.ToList();

                return View(EditMall);
            }
           
            return View("MallOwnerMallList");
        }

        [HttpPost]
        public ActionResult MallOwnerMallEdit(MallViewModel PostModel, HttpPostedFileBase MyImage)
        {
            var MallUpdate = db.MallTbls.Where(x => x.MallID == PostModel.MallID).FirstOrDefault();

            if (MallUpdate != null)
            {
                MallUpdate.MallName = PostModel.MallName;
                MallUpdate.SQFT = PostModel.SQFT;
                MallUpdate.NRooms = PostModel.NRooms;
                MallUpdate.NFloor = PostModel.NFloor;
                MallUpdate.MallCategoryID = PostModel.MallCategoryID;
                MallUpdate.LicenceNo = PostModel.LicenceNo;
                MallUpdate.Address = PostModel.Address;
                MallUpdate.PhnNo = PostModel.PhnNo;
                db.SaveChanges();

                //for image upload--------------------------------------------
                if (MyImage != null && MyImage.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(MyImage.FileName);

                    fileName = MallUpdate.MallID + "-Mall-Image-" + fileName;

                    var path = Path.Combine(Server.MapPath("~/Uploads/Mall"), fileName);

                    MyImage.SaveAs(path); //image store in server

                    MallUpdate.Img = fileName; //imgage path,db
                }
                db.SaveChanges();
                //------------------------------------------------------------------

                TempData["Message"] = "Saved Successfully!";
                return RedirectToAction("MallOwnerMallList");

            }
            TempData["Message"] = "Record Not Found!";
            return View("MallOwnerMallList");
        }


        /* [HttpPost]
          [Authorize(Roles = "MallOwner")]
          public ActionResult MallCancel(int id)
          {

              var MallOwnerID = User.Identity.GetUserId();
              MallViewModel model = new MallViewModel();
              model.MallTbls = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).OrderBy(m => m.MallID).ToList();
              model.SelectedMall = db.MallTbls.Find(id);
              model.DisplayMode = "ReadOnly";
              return View("MallOwnerMallList", model);

          }
          */



        //Employee Profile Edit...

        public ActionResult EmpProfileEdit(string MallEmpID)
        {
            var CheckDetails = db.MallEmpDetailsTbls.Where(x => x.MallEmpID == MallEmpID).FirstOrDefault();
            var MallEmpID2 = CheckDetails.MallEmpID;
            if (CheckDetails != null)
            {
                
                if(User.IsInRole("MallOwner"))
                {
                    var MallOwnerID = User.Identity.GetUserId();
                    ViewBag.EmpPostVar = db.EmpPostTbls.ToList();
                    ViewBag.MallVar = db.MallTbls.Where(x=>x.MallOwnerID==MallOwnerID).ToList();
                }
                MallEmpViewModel EditEmp = new MallEmpViewModel();
                EditEmp.MallEmpID = CheckDetails.MallEmpID;
                EditEmp.EmpName = CheckDetails.EmpName;
                EditEmp.MallID = CheckDetails.MallID;
                EditEmp.EmpPostID = CheckDetails.EmpPostID;
                EditEmp.Salary = CheckDetails.Salary;
                EditEmp.Address = CheckDetails.Address;
                EditEmp.EmailID = CheckDetails.EmailID;
                EditEmp.DOB = Convert.ToDateTime(CheckDetails.DOB).ToString("dd-MM-yyyy");
                
                EditEmp.Age = CheckDetails.Age;
                EditEmp.Gender = CheckDetails.Gender;
                EditEmp.PhnNo = CheckDetails.PhnNo;
                EditEmp.Img = CheckDetails.Img;
                return View(EditEmp);
            }
          
                return View("EmpProfileView", new { MallEmpID = MallEmpID });
         
        }

        [HttpPost]
        public ActionResult EmpProfileEdit(MallEmpViewModel PostModel, HttpPostedFileBase MyImage)
        {
            var EmpUpdate = db.MallEmpDetailsTbls.Where(x => x.MallEmpID == PostModel.MallEmpID).FirstOrDefault();
            var MallEmpID = PostModel.MallEmpID;
            if (EmpUpdate != null)
            {
                if (User.IsInRole("MallOwner"))
                {
                    EmpUpdate.MallID = PostModel.MallID;
                    EmpUpdate.EmpPostID = PostModel.EmpPostID;
                    EmpUpdate.Salary = PostModel.Salary;
                }
                else
                {
                    EmpUpdate.EmpName = PostModel.EmpName;
                    EmpUpdate.Address = PostModel.Address;
                    EmpUpdate.EmailID = PostModel.EmailID;
                    if (PostModel.DOB != null && PostModel.DOB != "")
                    {
                        var Date = DateTime.ParseExact(PostModel.DOB, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        EmpUpdate.DOB = Date;
                    }
                    // EmpUpdate.DOB = PostModel.DOB;
                    EmpUpdate.Age = PostModel.Age;
                    EmpUpdate.Gender = PostModel.Gender;
                    EmpUpdate.PhnNo = PostModel.PhnNo;
                    db.SaveChanges();
                    //img upload.-----------------------------------------------------------------
                    if (MyImage != null && MyImage.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(MyImage.FileName);

                        fileName = EmpUpdate.MallEmpID + "-MallEmp-Image-" + fileName;

                        var path = Path.Combine(Server.MapPath("~/Uploads/MallEmp"), fileName);

                        MyImage.SaveAs(path); //image store in server

                        EmpUpdate.Img = fileName; //imgage path,db
                    }
                }
                db.SaveChanges();
                //-----------------------------------------------------------------------------
                TempData["Message"] = "Saved Successfully!";
                return RedirectToAction("EmpProfileView",new {MallEmpID=MallEmpID});
              

            }
            TempData["Message"] = "Record Not Found!";
            return View("EmpProfileView",new { MallEmpID = MallEmpID });
            
          
        }


        //MallOwner Profile Edit
        [Authorize(Roles = "MallOwner")]
        public ActionResult MallOwnerProfileEdit(string MallOwnerID)
        {
            var CheckDetails = db.MallOwnerDetailsTbls.Where(x => x.MallOwnerID == MallOwnerID).FirstOrDefault();
            if (CheckDetails != null)
            {
               
                MallOwnerViewModel EditMallOwner = new MallOwnerViewModel();
                EditMallOwner.MallOwnerID = MallOwnerID;

                EditMallOwner.OwnerName = CheckDetails.OwnerName;
                EditMallOwner.Address = CheckDetails.Address;
                EditMallOwner.EmailID = CheckDetails.EmailID;
                EditMallOwner.DOB = Convert.ToDateTime(CheckDetails.DOB).ToString("dd-MM-yyyy");
                EditMallOwner.Age = CheckDetails.Age;
                EditMallOwner.Gender = CheckDetails.Gender;
                EditMallOwner.PhnNo = CheckDetails.PhnNo;
                EditMallOwner.Img = CheckDetails.Img;


                return View(EditMallOwner);
            }

            return View("MallOwnerProfileView");
        }

        [HttpPost]
        public ActionResult MallOwnerProfileEdit(MallOwnerViewModel PostModel, HttpPostedFileBase MyImage)
        {
            var MallOwnerUpdate = db.MallOwnerDetailsTbls.Where(x => x.MallOwnerID == PostModel.MallOwnerID).FirstOrDefault();

            if (MallOwnerUpdate != null)
            {


                MallOwnerUpdate.OwnerName = PostModel.OwnerName;
                MallOwnerUpdate.Address = PostModel.Address;
                MallOwnerUpdate.EmailID = PostModel.EmailID;
                if (PostModel.DOB != null && PostModel.DOB != "")
                {
                    var Date = DateTime.ParseExact(PostModel.DOB, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    MallOwnerUpdate.DOB = Date;
                }
                // MallOwnerUpdate.DOB = PostModel.DOB;
                MallOwnerUpdate.Age = PostModel.Age;
                MallOwnerUpdate.Gender = PostModel.Gender;
                MallOwnerUpdate.PhnNo = PostModel.PhnNo;
               

                db.SaveChanges();

                //img upload-----------------------------------------------------
                if (MyImage != null && MyImage.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(MyImage.FileName);

                    fileName = MallOwnerUpdate.MallOwnerID + "-MallOwner-Image-" + fileName;

                    var path = Path.Combine(Server.MapPath("~/Uploads/MallOwner"), fileName);

                    MyImage.SaveAs(path); //image store in server

                    MallOwnerUpdate.Img = fileName; //imgage path,db
                }
                db.SaveChanges();
                //-------------------------------------------------------------------------------
                TempData["Message"] = "Saved Successfully!";
                return RedirectToAction("MallOwnerProfileView");

            }
            TempData["Message"] = "Record Not Found!";
            return View("MallOwnerProfileView");
        }

        #endregion


        #region Message Actions.......
        #region Message of Admin and mallOwner....


        public ActionResult ComplaintToAdmin()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            return View();
        }
        [HttpPost]
        public ActionResult SendMsgAdminOwnerPartial(AdminMallMsgModel DetailsPost)
        {
            AdminMallMsgTbl NewAdd = new AdminMallMsgTbl();
            NewAdd.MallID = DetailsPost.MallID;
            NewAdd.WhoStatus = false;
            NewAdd.DatePosted = DateTime.Now;
            NewAdd.Msg = DetailsPost.Msg;
            db.AdminMallMsgTbls.Add(NewAdd);
            db.SaveChanges();
            TempData["Message"] = "Send Successfully!";
            return RedirectToAction("ComplaintToAdmin");
        }
        #endregion

        #region Message of MallOwner and Employee---

        //for sending msg by mall owner.....
        public ActionResult SendOwnerEmpMsg(string MallEmpID)
        {
            // var Model = db.MallEmpMsgTbls.Where(x => x.MallEmpDetailsID == MallEmpDetailsID).FirstOrDefault();
            MallEmpMsgModel id = new MallEmpMsgModel();//to clear... 
            id.MallEmpID = MallEmpID;
            lock (lockObj)
            {
                loc1 = Request.UrlReferrer.ToString();//assigning url linked to this url
                ViewBag.loc1 = loc1;
            }
            return View(id);
        }

        //for sending complaints by employee.....
        public ActionResult ComplaintToMallOwner()
        {
            
            return View();
        }

        //database entering action...
        [HttpPost]
        public ActionResult SendOwnerEmpMsg(MallEmpMsgModel DetailsPost)
        {
            MallEmpMsgTbl NewAdd = new MallEmpMsgTbl();
            if (User.IsInRole("MallOwner"))
            {
                NewAdd.MallEmpID = DetailsPost.MallEmpID;
                NewAdd.WhoStatus = true;
            }
            else if (User.IsInRole("MallEmp"))
            {
                var MallEmpID = User.Identity.GetUserId();
                NewAdd.MallEmpID = MallEmpID;
                NewAdd.WhoStatus = false;
            }
            NewAdd.DatePosted = DateTime.Now;
            NewAdd.Msg = DetailsPost.Msg;
            db.MallEmpMsgTbls.Add(NewAdd);
            db.SaveChanges();
            TempData["Message"] = "Send Successfully!";
            if (User.IsInRole("MallOwner"))
            {
              return Redirect(loc1);
                //return Redirect(Request.UrlReferrer.ToString());

            }
            else
            {

                return Redirect(Request.UrlReferrer.ToString());
            }
            

        }

        //for viewing meggases...............
        public ActionResult RemarkToEmpPage()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            return View();
        }

        //MallOwnerEmployee Msg Details View..
        public ActionResult MallEmpMsgDetails(int id)
        {
            var Model = db.MallEmpMsgTbls.Where(x => x.MallEmpMsgID == id).FirstOrDefault();
            MallEmpMsgTbl NewAdd = db.MallEmpMsgTbls.Where(x => x.MallEmpMsgID == id).FirstOrDefault();
            if (User.IsInRole("MallOwner"))
            {
                if (Model.WhoStatus == false)
                {
                    NewAdd.ReadStatus = true;
                    db.SaveChanges();
                }
            }
            else
            {
                if (Model.WhoStatus == true)
                {
                    NewAdd.ReadStatus = true;
                    db.SaveChanges();
                }
            }
            
            return View(Model);
        }
        #endregion
        #endregion//end of all msg actions

        #region Payment Actions...



        public ActionResult PayEmpSalarySearch()//for searching employee
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            ViewBag.EmpPostVar = db.EmpPostTbls.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult PayEmpSalarySearch(MallEmpViewModel DetailsPost)
        {

            int MallID = DetailsPost.MallID;
            String Post = DetailsPost.Post;
            String EmpName = DetailsPost.EmpName;
            return RedirectToAction("PayEmpSalarySearch", new { MallID = MallID, Post = Post, EmpName = EmpName });
        }

        public ActionResult PayEmpSalary(string MallEmpID)//for payment after searching
        {
            MallEmpPayModel id = new MallEmpPayModel();//to clear... 
            id.MallEmpID = MallEmpID;
            return View(id);
        }

        [HttpPost]
        public ActionResult PayEmpSalary(MallEmpPayModel DetailsPost)
        {

            string MallEmpID = DetailsPost.MallEmpID;
            string PaymentMode = DetailsPost.PaymentMode;
            return RedirectToAction("PayEmpSalarysearch", new { PaymentMode = PaymentMode, MallEmpID = MallEmpID });



        }
        [HttpPost]
        public ActionResult PayEmpSalary2(MallEmpPayModel DetailsPost)
        {

            MallEmpPayTbl NewAdd = new MallEmpPayTbl();
            NewAdd.MallEmpID = DetailsPost.MallEmpID;
            NewAdd.PaymentMode = DetailsPost.PaymentMode;
            NewAdd.DatePaid = DateTime.Now;
            NewAdd.Amount = DetailsPost.Amount;
            db.MallEmpPayTbls.Add(NewAdd);
            db.SaveChanges();
            ViewBag.Completed = "Transaction Completed!";
            return View();
        }

        #endregion

      
    }
}
