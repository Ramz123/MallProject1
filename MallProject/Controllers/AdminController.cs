﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MallProject.Models;
using static MallProject.Models.ViewModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using iTextSharp.text.html.simpleparser;

namespace MallProject.Controllers
{
    public class AdminController : Controller
    {

        MallManagementEntities db = new MallManagementEntities();

        //public void PrintBill(int AdminMallTransactionID)
        //{
        //    var GetTransactionDetails = db.AdminMallTransactionTbls.Where(x => x.AdminMallTransactionID == AdminMallTransactionID).FirstOrDefault();

        //    string HTMLContent = "Hello <b style='color:red'>World</b>";
        //    HTMLContent += "<br>Amount = " + GetTransactionDetails.Amount + " $";
        //    HTMLContent += "<table></table>";

        //    Response.Clear();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=" + "PDFfile.pdf");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.BinaryWrite(GetPDF(HTMLContent));
        //    Response.End();
        //}
        #region Generate Slip...

        public ActionResult GetSlip(int AdminMallTransactionID)
        {
            var GetSlip = db.AdminMallTransactionTbls.Where(x => x.AdminMallTransactionID == AdminMallTransactionID).FirstOrDefault();

            return View(GetSlip);

        }
        #endregion
        #region For PDF Print............

        //public void DownloadPDF(int AdminMallTransactionID)
        //{


        //    var Bill = db.AdminMallTransactionTbls.Where(x => x.AdminMallTransactionID == AdminMallTransactionID).FirstOrDefault();
        //    var MallList = db.MallTbls.ToList();


        //    string html = "";
        //    html += "<h1 align=center>PAYMENT RECEIPT</h1>";
        //    html += "<---------------------------------------------------------------------------------------------------------------------------------------";
        //    html += "<br><br><br><br><b> Transaction ID:" + Bill.AdminMallTransactionID + "</b><br>";
        //    html += "<b>  Date:" + "</b>" + Bill.DatePaid + "<br>";
        //    foreach (var id in MallList)
        //    {
        //        if (Bill.MallID == id.MallID)
        //        {


        //            html += "<b>  Mobile:" + "</b>" + id.PhnNo + "<br>";

        //        }
        //    }

        //    foreach (var id in MallList)
        //    {
        //        if (Bill.MallID == id.MallID)
        //        {
        //            html += "<b> Mall Name:" + "</b>" + id.MallName + "</br>";
        //        }
        //    }
        //    html += "</br>---------------------------------------------------------------------------------------------------------------------------------------";
        //    html += "<br><br><br><br><br><br><table><b><tr><td>DESCRIPTION</td><td>AMOUNT</b></td></tr>";
        //    html += "<tr><td>" + "Payment For" + Bill.DatePaid + "</td><td>" + Bill.Amount + "</td><td>";


        //    html += "<tr><td></td><td></td><td><b>Grand Total</b></td><td>" + Bill.Amount + "</td></tr></table>";
        //    string HTMLContent = html;
        //    Response.Clear();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=" + "PDFfile.pdf");
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.BinaryWrite(GetPDF(HTMLContent));
        //    Response.End();
        //}

        //public byte[] GetPDF(string pHTML)
        //{
        //    byte[] bPDF = null;

        //    MemoryStream ms = new MemoryStream();
        //    TextReader txtReader = new StringReader(pHTML);

        //    // 1: create object of a itextsharp document class  
        //    Document doc = new Document(PageSize.A4, 25, 25, 25, 25);

        //    // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
        //    PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

        //    // 3: we create a worker parse the document  
        //    HTMLWorker htmlWorker = new HTMLWorker(doc);

        //    // 4: we open document and start the worker on the document  
        //    doc.Open();
        //    htmlWorker.StartDocument();


        //    // 5: parse the html into the document  
        //    htmlWorker.Parse(txtReader);

        //    // 6: close the document and the worker  
        //    htmlWorker.EndDocument();
        //    htmlWorker.Close();
        //    doc.Close();

        //    bPDF = ms.ToArray();

        //    return bPDF;
        //}
        #endregion

        #region Main Page and login index action...
        public ActionResult MainPage()
        {
            var MainPage = db.MallCategoryTbls.ToList();
            return View(MainPage);

        }
        public ActionResult AdminIndex()
        {

            return View();

        }
        #endregion

        #region login action no need..
        public ActionResult AdminLogin()
        {

            return View();

        }
        [HttpPost]
        public ActionResult AdminLogin(MallOwnerViewModel DetailsPost)
        {

            return RedirectToAction("AdminIndex");
        }

        #endregion

        #region table view actions..
        public ActionResult AdminMallListView()
        {
            var AdminMallListView = db.MallTbls.OrderByDescending(x => x.RegDate).ToList();
            return View(AdminMallListView);

        }

        public ActionResult AdminMallOwnerListView()
        {
            var AdminMallOwnerListView = db.MallOwnerDetailsTbls.OrderByDescending(x => x.RegDate).ToList();
            return View(AdminMallOwnerListView);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult OwnerDetailsViewForAdmin(int MallOwnerDetailsID)
        {


            var OwnerDetailsViewForAdmin = db.MallOwnerDetailsTbls.Where(x => x.MallOwnerDetailsID == MallOwnerDetailsID).FirstOrDefault();

            return View(OwnerDetailsViewForAdmin);

        }

        public ActionResult AdminMallListViewOnID(string MallOwnerID, string OwnerName)
        {

            var AdminMallListViewOnID = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).OrderByDescending(x => x.RegDate).ToList();
            ViewBag.OwnerName = OwnerName;
            string pre2 = Request.UrlReferrer.ToString();//for geting url linked to this url

            return View(AdminMallListViewOnID);
        }

//transaction list view................................................................
         public ActionResult ViewAdminOwnerTransaction()
        {
            if (User.IsInRole("MallOwner"))
            {
                var MallOwnerID = User.Identity.GetUserId();
                var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
                ViewBag.Mallvar = MallList;               
                return View();
            }
            else
            {
                var MallList = db.MallTbls.ToList();
                ViewBag.Mallvar = MallList;
                if (MallList.Count() > 0)
                {
                    return View();
                }
                else
                {
                    TempData["Message"] = "No Records!";
                    return RedirectToAction("AdminIndex", "Admin");
                }
               
            }

        }
        [HttpPost]
        public ActionResult ViewAdminOwnerTransaction(AdminMallTransactionModel DetailsPost)
        {
            int MallID = DetailsPost.MallID;          
            return RedirectToAction("ViewAdminOwnerTransaction", new { MallID = MallID});
        }
        //.......................................................................................

        public ActionResult MallSite(String MallName)
        {
            var abc = MallName;
            return RedirectToAction("MallSite", "MallOwner", new { MallName = abc });
        }

        #endregion

        #region Jqry (no need)
        public String SearchMallOwner()
        {
            string limit, start, searchKey, orderColumn, orderDir, draw, jsonString;
            limit = Request.QueryString["length"] == null ? "" : Request.QueryString["length"].ToString();
            start = Request.QueryString["start"] == null ? "" : Request.QueryString["start"].ToString();
            searchKey = Request.QueryString["search[value]"] == null ? "" : Request.QueryString["search[value]"].ToString();
            orderColumn = Request.QueryString["order[0][column]"] == null ? "" : Request.QueryString["order[0][column]"].ToString();
            orderDir = Request.QueryString["order[0][dir]"] == null ? "" : Request.QueryString["order[0][dir]"].ToString();
            draw = Request.QueryString["draw"] == null ? "" : Request.QueryString["draw"].ToString();
            var parameter = new List<object>();
            var param = new SqlParameter("@orderColumn", orderColumn);
            parameter.Add(param);
            param = new SqlParameter("@limit", limit);
            parameter.Add(param);
            param = new SqlParameter("@orderDir", orderDir);
            parameter.Add(param);
            param = new SqlParameter("@start", start);
            parameter.Add(param);
            param = new SqlParameter("@searchKey", searchKey);
            parameter.Add(param);
            var UserSearchList = db.Database.SqlQuery<CompanySearch>("EXEC MallSearch @orderColumn,@limit,@orderDir,@start,@searchKey ", parameter.ToArray()).ToList();
            dynamic newtonresult = new
            {
                status = "success",
                draw = Convert.ToInt32(draw == "" ? "0" : draw),
                recordsTotal = UserSearchList.FirstOrDefault().TotalCount,
                recordsFiltered = UserSearchList.FirstOrDefault().TotalCount,
                data = UserSearchList
            };
            jsonString = JsonConvert.SerializeObject(newtonresult);
            return jsonString;
        }
        private class CompanySearch
        {
            public int TotalCount { get; set; }
            public string abc { get; set; }
            public int MallOwnerDetailsID { get; set; }
            public string MallOwnerID { get; set; }
            public string OwnerName { get; set; }
            public string Address { get; set; }
            public string EmailID { get; set; }
            public Nullable<System.DateTime> DOB { get; set; }
            public Nullable<int> Age { get; set; }
            public string PhnNo { get; set; }
        }


        #endregion

        #region category view or add...
        public ActionResult ViewCategory()
        {
            var ViewCategory = db.MallCategoryTbls.ToList();
            return View(ViewCategory);
        }
        public ActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCategory(CategoryViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                MallCategoryTbl NewAdd = new MallCategoryTbl();
                NewAdd.MallCategoryName = DetailsPost.MallCategoryName;
                NewAdd.Name = DetailsPost.Name;
                NewAdd.Amount = DetailsPost.Amount;
                db.MallCategoryTbls.Add(NewAdd);
                db.SaveChanges();
                TempData["Message"] = "Added Successfully!";//for alert message
                return RedirectToAction("ViewCategory");
            }
            return View();
        }
        #endregion

        #region category edit actions..
        [Authorize(Roles = "Admin")]
        public ActionResult EditCategory(int MallCategoryID)
        {
            var CheckCategory = db.MallCategoryTbls.Where(x => x.MallCategoryID == MallCategoryID).FirstOrDefault();
            if (CheckCategory != null)
            {
                CategoryViewModel EditCategory = new CategoryViewModel();
                EditCategory.MallCategoryName = CheckCategory.MallCategoryName;
                EditCategory.Name = CheckCategory.Name;
                EditCategory.Amount = CheckCategory.Amount;
                return View(EditCategory);
            }

            return View("ViewCategory");
        }
        [HttpPost]
        public ActionResult EditCategory(CategoryViewModel PostModel)
        {
            if (ModelState.IsValid)
            {
                var CategoryUpdate = db.MallCategoryTbls.Where(x => x.MallCategoryID == PostModel.MallCategoryID).FirstOrDefault();

                if (CategoryUpdate != null)
                {
                    CategoryUpdate.MallCategoryName = PostModel.MallCategoryName;
                    CategoryUpdate.Name = PostModel.Name;
                    CategoryUpdate.Amount = PostModel.Amount;
                    db.SaveChanges();
                    TempData["Message"] = "Saved Successfully!";
                    return RedirectToAction("ViewCategory");
                }

                return View("ViewCategory");
            }
            return View();
        }
        #endregion

        #region Remove Actions..
        public ActionResult RemoveMall(int MallID)
        {
            MallTbl p = db.MallTbls.Find(MallID);
            db.MallTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            return RedirectToAction("AdminMallListView", new { MallID = 0 });
        }
        public ActionResult RemoveMallOwner(int MallOwnerDetailsID)
        {
            MallOwnerDetailsTbl p = db.MallOwnerDetailsTbls.Find(MallOwnerDetailsID);
            db.MallOwnerDetailsTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";

            return RedirectToAction("AdminMallOwnerListView", new { MallOwnerDetailsID = 0 });
        }

        public ActionResult RemoveCategory(int MallCategoryID)
        {
            MallCategoryTbl p = db.MallCategoryTbls.Find(MallCategoryID);
            db.MallCategoryTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            return RedirectToAction("ViewCategory", new { MallCategoryID = 0 });
        }



        //for removing message from view.....
        [HttpPost]
        public ActionResult RemoveMsg(int[] ids)
        {


            foreach (var id in ids)
            {
                AdminMallMsgTbl NewAdd = db.AdminMallMsgTbls.Find(id);
                NewAdd.AdminViewStatus = false;

                db.SaveChanges();

                AdminMallMsgTbl p = db.AdminMallMsgTbls.Find(id);
                if (NewAdd.MallViewStatus == NewAdd.AdminViewStatus)
                {
                    db.AdminMallMsgTbls.Remove(p);
                    db.SaveChanges();

                }
            }
            TempData["Message"] = "Deleted Successfully!";
          
           return Redirect(Request.UrlReferrer.ToString());
            //return Redirect(Request.UrlReferrer.AbsolutePath);

        }
        [HttpPost]
        public ActionResult RemoveAdminMallTransaction(int[] ids)
        {


            foreach (var id in ids)
            {
                AdminMallTransactionTbl NewAdd = db.AdminMallTransactionTbls.Find(id);
                if (User.IsInRole("Admin"))
                {

                    NewAdd.AdminViewStatus = false;
                }
                else
                {
                    NewAdd.MallViewStatus = false;
                }

                db.SaveChanges();

                AdminMallTransactionTbl p = db.AdminMallTransactionTbls.Find(id);
                if (NewAdd.MallViewStatus == NewAdd.AdminViewStatus)
                {
                    db.AdminMallTransactionTbls.Remove(p);
                    db.SaveChanges();

                }
            }
            TempData["Message"] = "Deleted Successfully!";
            //return RedirectToAction("ViewAdminOwnerTransaction");
            return Redirect(Request.UrlReferrer.ToString());

        }
        #endregion

        #region Message to Mall Owner....
        //msg page
        public ActionResult RemarkToMallOwner()
        {
            var MallList = db.MallTbls.ToList();
            ViewBag.Mallvar = MallList;
            return View();
        }
        //  for sending reply to mall
        public ActionResult SendMsgAdminOwnerReply(int MallID)
        {
            AdminMallMsgModel id = new AdminMallMsgModel();//to clear... 
            id.MallID = MallID;
            return View(id);
        }
        [HttpPost]
        public ActionResult SendMsgAdminOwnerPartial(AdminMallMsgModel DetailsPost)
        {
            AdminMallMsgTbl NewAdd = new AdminMallMsgTbl();
            NewAdd.MallID = DetailsPost.MallID;
            NewAdd.WhoStatus = true;
            NewAdd.DatePosted = DateTime.Now;
            NewAdd.Msg = DetailsPost.Msg;
            db.AdminMallMsgTbls.Add(NewAdd);
            db.SaveChanges();
            TempData["Message"] = "Send Successfully!";
            return RedirectToAction("RemarkToMallOwner");
        }
        //Admin MallOwner Msg Details View..
        public ActionResult AdminMallMsgDetails(int id)
        {

            var Model = db.AdminMallMsgTbls.Where(x => x.AdminMallMsgID == id).FirstOrDefault();
            AdminMallMsgTbl NewAdd = db.AdminMallMsgTbls.Where(x => x.AdminMallMsgID == id).FirstOrDefault();
            if (User.IsInRole("Admin"))
            {
                if (Model.WhoStatus == false)
                {
                    NewAdd.ReadStatus = true;
                    db.SaveChanges();
                }
            }
            else
            {
                if (Model.WhoStatus == true)
                {
                    NewAdd.ReadStatus = true;
                    db.SaveChanges();
                }
            }

            return View(Model);
        }



        #endregion

        #region Payment Actions...
        public ActionResult PayAdminRent()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            return View();
        }

        [HttpPost]
        public ActionResult PayAdminRent(AdminMallTransactionModel DetailsPost)
        {

            int MallID = DetailsPost.MallID;
            String PaymentMode = DetailsPost.PaymentMode;
            int AdminMallTransactionID = DetailsPost.AdminMallTransactionID;
            return RedirectToAction("PayAdminRent", new { MallID = MallID, PaymentMode = PaymentMode, AdminMallTransactionID = AdminMallTransactionID });

            //var MallOwnerID = User.Identity.GetUserId();
            //var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            //ViewBag.Mallvar = MallList;
            //return View();

        }
        [HttpPost]
        public ActionResult PayAdminRent2(AdminMallTransactionModel DetailsPost)
        {


            AdminMallTransactionTbl NewAdd = new AdminMallTransactionTbl();
            NewAdd.MallID = DetailsPost.MallID;
            NewAdd.PaymentMode = DetailsPost.PaymentMode;
            NewAdd.DatePaid = DateTime.Now;
            NewAdd.Amount = DetailsPost.Amount;
            db.AdminMallTransactionTbls.Add(NewAdd);
            db.SaveChanges();
            var ID = db.AdminMallTransactionTbls.Where(x => x.MallID == DetailsPost.MallID).FirstOrDefault();
            ViewBag.AdminMallTransactionID = ID.AdminMallTransactionID;
            ViewBag.Completed = "Transaction Completed!";
            return View();

        }
        #endregion


        public ActionResult Sample()
        {
            return View();
        }


    }


}
